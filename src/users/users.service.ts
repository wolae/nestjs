import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { UpdateResult, DeleteResult } from  'typeorm';

@Injectable()
export class UsersService {

    constructor(@InjectRepository(User) private usersRepository: Repository<User>) { }

    async  findAll(): Promise<User[]> {
        return await this.usersRepository.find();
    }
    async  findById(id): Promise<User[]> {
        return await this.usersRepository.findByIds(id);
    }

    async  create(user: User): Promise<User> {
        return await this.usersRepository.save(user);
    }

    async update(user: User): Promise<UpdateResult> {
        return await this.usersRepository.update(user.id, user);
    }

    async delete(id): Promise<DeleteResult> {
        return await this.usersRepository.delete(id);
    }
}