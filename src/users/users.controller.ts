import { Controller, Post, Body, Get, Put, Delete,Param} from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from './user.entity';
import { ApiParam} from '@nestjs/swagger';

@Controller('users')
export class UsersController {

    constructor(private service: UsersService) { }

    @Get()
    index(): Promise<User[]> {
        return this.service.findAll();
      } 
    @Get(':id')
    @ApiParam({ name: 'id', required: true})
    async get(@Param('id') id): Promise<any>{
        return this.service.findById(id);
    }   

    @Post('create')
    async create(@Body() user: User): Promise<any> {
      return this.service.create(user);
    }  

    @Put(':id/update')
    async update(@Param('id') id, @Body() user: User): Promise<any> {
        user.id = Number(id);
        console.log('Update #' + user.id)
        return this.service.update(user);
    }  

    @Delete(':id/delete')
    async delete(@Param('id') id): Promise<any> {
      return this.service.delete(id);
    }  
}